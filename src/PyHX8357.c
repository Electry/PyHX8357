#include <Python.h>
#include <bcm2835.h>
#include <sys/time.h>
#include <math.h>

#define LCD_WIDTH      480
#define LCD_HEIGHT     320


#define MODE_DATA      1
#define MODE_COMMAND   0


#define MADCTL_RGB     0x00  // Red-Green-Blue pixel order
#define MADCTL_MH      0x04  // LCD refresh right to left
#define MADCTL_BGR     0x08  // Blue-Green-Red pixel order
#define MADCTL_ML      0x10  // LCD refresh Bottom to top
#define MADCTL_MV      0x20  // Reverse Mode (90 rotation)
#define MADCTL_MX      0x40  // Right to left
#define MADCTL_MY      0x80  // Bottom to top


#define HX8357_SWRESET 0x01  // Software reset

#define HX8357_SLPIN   0x10  // Sleep IN
#define HX8357_SLPOUT  0x11  // Sleep OUT

#define HX8357_DISPON  0x29  // Display ON
#define HX8357_CASET   0x2A  // Column ADDR set
#define HX8357_PASET   0x2B  // Row ADDR set
#define HX8357_RAMWR   0x2C  // Write to RAM

#define HX8357_PIXFMT  0x3A  // Pixel Format
#define HX8357_MADCTL  0x36  // Memory Access Control

#define HX8357_IMCTL   0xB0  // Interface Mode Control

#define HX8357_GMCTRP  0xE0  // Positive Gamma Control
#define HX8357_GMCTRN  0xE1  // Negative Gamma Control
#define HX8357_GMCTRD  0xE2  // Digital Gamma Control

#define HX8357_PWR3    0xC2  // Power Control 3
#define HX8357_VCOM1   0xC5  // VCOM Control 1


static unsigned int g_GPIO_cd    = 0; // Command/data mode
static unsigned int g_GPIO_reset = 0; // Hardware reset


static int _delayms(int ms) {
    struct timespec tim, timr;
    tim.tv_sec = 0;
    tim.tv_nsec = (long)(ms * 1000000L);
    return nanosleep(&tim, &timr);
}

static PyObject *PyHX8357_init(PyObject *self, PyObject *args) {
    unsigned int device;
    int ret;

    if (!PyArg_ParseTuple(args, "III", &device, &g_GPIO_cd, &g_GPIO_reset)) {
        return NULL;
    }

    ret = bcm2835_init();
    if (!ret)
        return NULL;

    ret = bcm2835_spi_begin();
    if (!ret)
        return NULL;

    bcm2835_spi_setBitOrder(BCM2835_SPI_BIT_ORDER_MSBFIRST);
    bcm2835_spi_setDataMode(BCM2835_SPI_MODE0);
    bcm2835_spi_setClockDivider(BCM2835_SPI_CLOCK_DIVIDER_8);

    bcm2835_spi_chipSelect(device);
    bcm2835_spi_setChipSelectPolarity(device, LOW);

	bcm2835_gpio_fsel(g_GPIO_cd, BCM2835_GPIO_FSEL_OUTP);
	bcm2835_gpio_fsel(g_GPIO_reset, BCM2835_GPIO_FSEL_OUTP);

    return Py_BuildValue("i", ret);
}

static PyObject *PyHX8357_close(PyObject *self) {
    bcm2835_spi_end();
    int ret = bcm2835_close();

    return Py_BuildValue("i", ret);
}

static void _set_mode(uint8_t mode) {
	bcm2835_gpio_write(g_GPIO_cd, mode);
}

static void _send_cmd(uint8_t byte) {
	char buf = byte;

	_set_mode(MODE_COMMAND);
	bcm2835_spi_writenb(&buf, 1);
	_set_mode(MODE_DATA);
}

static void _send_data8(uint8_t bytes[], size_t len) {
	char buf;
	for (int i = 0; i < len; i++) {
		buf = bytes[i];
		bcm2835_spi_writenb(&buf, 1);
	}
}

static void _send_data16(uint16_t bytes[], size_t len) {
	char buf[len * 2];
	memcpy(buf, bytes, len * 2);
	bcm2835_spi_writenb(buf, len * 2);
}

static void _hw_reset(void) {
	bcm2835_gpio_write(g_GPIO_reset, 1);
	_delayms(100);
	bcm2835_gpio_write(g_GPIO_reset, 0);
	_delayms(100);
	bcm2835_gpio_write(g_GPIO_reset, 1);
	_delayms(200);
}

static void _sw_reset(void) {
	_send_cmd(HX8357_SWRESET);
	_delayms(120);
}

static PyObject *PyHX8357_lcd_reset(PyObject *self) {
	_hw_reset();

	Py_RETURN_NONE;
}

static PyObject *PyHX8357_lcd_soft_reset(PyObject *self) {
	_sw_reset();

	Py_RETURN_NONE;
}

static PyObject *PyHX8357_lcd_wake(PyObject *self) {
    _send_cmd(HX8357_SLPOUT);

    Py_RETURN_NONE;
}

static PyObject *PyHX8357_lcd_sleep(PyObject *self) {
    _send_cmd(HX8357_SLPIN);

    Py_RETURN_NONE;
}

static PyObject *PyHX8357_lcd_init(PyObject *self) {

    // Hardware reset
	_hw_reset();

	// Software reset
	_sw_reset();
	
	// Interface Mode Control
	_send_cmd(HX8357_IMCTL);
	_send_data8((uint8_t[]){0x00}, 1);

	// Sleep OUT
	_send_cmd(HX8357_SLPOUT);
	_delayms(250);

	// Pixel format
	_send_cmd(HX8357_PIXFMT);
	_send_data8((uint8_t[]){0x55}, 1);

	// Power Control 3
	_send_cmd(HX8357_PWR3);
    _send_data8((uint8_t[]){0x44}, 1);

	// VCOM Control 1
    _send_cmd(HX8357_VCOM1);
    _send_data8((uint8_t[]){0x00, 0x00, 0x00, 0x00}, 4);

	// Positive Gamma Control
	_send_cmd(HX8357_GMCTRP);
	_send_data8((uint8_t[]){
		0x0F, 0x1F, 0x1C, 0x0C, 0x0F,
		0x08, 0x48, 0x98, 0x37, 0x0A,
		0x13, 0x04, 0x11, 0x0D, 0x00}, 15);

	// Negative Gamma Control
	_send_cmd(HX8357_GMCTRN);
	_send_data8((uint8_t[]){
		0x0F, 0x32, 0x2E, 0x0B, 0x0D,
		0x05, 0x47, 0x75, 0x37, 0x06,
		0x10, 0x03, 0x24, 0x20, 0x00}, 15);

	// Digital Gamma Control 1
	_send_cmd(HX8357_GMCTRD);
	_send_data8((uint8_t[]){
		0x0F, 0x32, 0x2E, 0x0B, 0x0D,
		0x05, 0x47, 0x75, 0x37, 0x06,
		0x10, 0x03, 0x24, 0x20, 0x00}, 15);

	// Memory Access Control (rotation)
	_send_cmd(HX8357_MADCTL);
	_send_data8((uint8_t[]){MADCTL_MV | MADCTL_BGR}, 1);
	
	// Display ON
	_send_cmd(HX8357_DISPON);
	_delayms(250);

    Py_RETURN_NONE;
}

static PyObject *PyHX8357_lcd_setbounds(PyObject *self, PyObject *args) {
    unsigned int x, y, w, h;

    if (!PyArg_ParseTuple(args, "IIII", &x, &y, &w, &h)) {
        return NULL;
    }

    _send_cmd(HX8357_CASET);
    _send_data8((uint8_t[]){ // write is 16b per value
		0xFF & (x >> 8),
		0xFF & x,
		0xFF & ((x + w - 1) >> 8),
		0xFF & (x + w - 1)},
		4);

    _send_cmd(HX8357_PASET);
    _send_data8((uint8_t[]){
		0xFF & (y >> 8),
		0xFF & y,
		0xFF & ((y + h - 1) >> 8),
		0xFF & (y + h - 1)},
		4);

    _send_cmd(HX8357_RAMWR);

    Py_RETURN_NONE;
}

static PyObject *PyHX8357_lcd_setpixels(PyObject *self, PyObject *args) {
    char *data;
    int len, i;
    uint8_t r, g, b;
	uint16_t rgb;

    if (!PyArg_ParseTuple(args, "s#", &data, &len)) {
        return NULL;
    }

    for (i = 0; i < len; i += 3) {
        r = (int)round(data[i] / 8);
        g = (int)round(data[i+1] / 4);
        b = (int)round(data[i+2] / 8);
        rgb = (r << 11) + (g << 5) + b;
		// Swap bytes
		rgb = (rgb >> 8) | ((rgb & 0xFF) << 8);

		_send_data16((uint16_t[]){rgb}, 1);
    }

    return Py_BuildValue("i", i);
}

static PyMethodDef PyHX8357_Methods[] = {
    {"init", (PyCFunction)PyHX8357_init, METH_VARARGS, "Initialize PyHX8357 library."},
    {"close", (PyCFunction)PyHX8357_close, METH_NOARGS, "Close PyHX8357 library."},

    {"lcd_init", (PyCFunction)PyHX8357_lcd_init, METH_NOARGS, "Prepare LCD display."},

	{"lcd_reset", (PyCFunction)PyHX8357_lcd_reset, METH_NOARGS, "HW Reset"},
    {"lcd_soft_reset", (PyCFunction)PyHX8357_lcd_soft_reset, METH_NOARGS, "SW Reset"},
	
    {"lcd_sleep", (PyCFunction)PyHX8357_lcd_sleep, METH_NOARGS, "Sleep IN"},
    {"lcd_wake", (PyCFunction)PyHX8357_lcd_wake, METH_NOARGS, "Sleep OUT"},

    {"lcd_setbounds", (PyCFunction)PyHX8357_lcd_setbounds, METH_VARARGS, "Set write bounds."},
    {"lcd_setpixels", (PyCFunction)PyHX8357_lcd_setpixels, METH_VARARGS, "Write RGB565 pixels on LCD."},

    {NULL, NULL, 0, NULL} /* Sentinel */
};

struct module_state {
    PyObject *error;
};

static int PyHX8357_Traverse(PyObject *m, visitproc visit, void *arg) {
    Py_VISIT(((struct module_state*)PyModule_GetState(m))->error);
    return 0;
}

static int PyHX8357_Clear(PyObject *m) {
    Py_CLEAR(((struct module_state*)PyModule_GetState(m))->error);
    return 0;
}

static struct PyModuleDef PyHX8357_Module = {
    PyModuleDef_HEAD_INIT,
    "PyHX8357",
    NULL,
    sizeof(struct module_state),
    PyHX8357_Methods,
    NULL,
    PyHX8357_Traverse,
    PyHX8357_Clear,
    NULL
};

PyMODINIT_FUNC PyInit_PyHX8357(void)
{
    PyObject *m;

    m = PyModule_Create(&PyHX8357_Module);
    if (m == NULL)
        return NULL;

    struct module_state *st = ((struct module_state*)PyModule_GetState(m));

    st->error = PyErr_NewException("PyHX8357.error", NULL, NULL);
    if (st->error != NULL) {
        Py_DECREF(m);
    }

    PyModule_AddObject(m, "error", st->error);
    return m;
}
