from distutils.core import setup, Extension

pyhx8357 = Extension('PyHX8357',
                      include_dirs=['/usr/local/include'],
                      libraries = ['bcm2835'],
                      library_dirs = ['/usr/local/lib'],
                      sources = ['src/PyHX8357.c'])

setup (name = 'PyHX8357',
       version = '0.0.1',
       description = 'Python extension for HX8357 based 480x320 LCD TFT SPI displays',
       author='Michal Chvila',
       author_email='michal@chvila.sk',
       license='GPLv2',
       keywords=["raspberry pi", "bcm2835", "hx8357"],
       classifiers=[
           "Programming Language :: Python",
           "Development Status :: 3 - Alpha",
           "Topic :: System :: Hardware"
       ],
       ext_modules = [pyhx8357])
